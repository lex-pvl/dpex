export default {
  target: 'static',
  head: {
    title: 'dpex',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.png'}
    ]
  },

  css: [
    '@/assets/styles/main.less'
  ],
  plugins: [
    {src: '~/plugins/select.js'},
    {src: '~/plugins/modal.js'},
    {
      src: '~/plugins/ymapPlugin.js',
      mode: 'client'
    }
  ],
  components: true,
  buildModules: [],
  modules: [
    '@nuxtjs/axios',
  ],
  axios: {},
  build: {}
}
